/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : db_srs_services

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-05-20 19:49:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `servers`
-- ----------------------------
DROP TABLE IF EXISTS `servers`;
CREATE TABLE `servers` (
  `device_id` varchar(20) NOT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `is_closed` tinyint(1) DEFAULT '0' COMMENT '是否关闭 1关闭 0未关闭 默认0未关闭',
  `update_time` int(11) DEFAULT NULL COMMENT '最近一次心跳汇报时间',
  `create_time` int(11) DEFAULT NULL COMMENT '第一次心跳汇报时间',
  `now_ms` bigint(13) DEFAULT NULL COMMENT '当前时间',
  PRIMARY KEY (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of servers
-- ----------------------------

-- ----------------------------
-- Table structure for `streams`
-- ----------------------------
DROP TABLE IF EXISTS `streams`;
CREATE TABLE `streams` (
  `name` char(32) NOT NULL COMMENT '流号',
  `app` varchar(10) DEFAULT NULL COMMENT '应用名',
  `vhost_id` int(11) DEFAULT NULL COMMENT '所属vhost 编号',
  `client_id` int(11) DEFAULT NULL COMMENT '推流客户端',
  `ip` varchar(20) DEFAULT NULL COMMENT '推流客户端IP地址',
  `start_time` int(11) DEFAULT NULL COMMENT '开始推流时间',
  `stop_time` int(11) DEFAULT NULL COMMENT '结束推流时间',
  `remak` text COMMENT '推流备注说明',
  `is_active` tinyint(1) DEFAULT NULL COMMENT '推流状态 0没有推流 1正在推流',
  `clients` int(11) DEFAULT '0' COMMENT '在线人数 默认0',
  `live_ms` bigint(20) DEFAULT NULL COMMENT '直播时长',
  `send_bytes` bigint(20) DEFAULT NULL COMMENT '已发送字节数据',
  `recv_bytes` bigint(20) DEFAULT NULL COMMENT '已接收字节数',
  `video` varchar(200) DEFAULT NULL COMMENT '视频编码',
  `audio` varchar(200) DEFAULT NULL COMMENT '音频编码信息',
  `create_time` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '审核状态  1通过 0不通过 默认 1',
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of streams
-- ----------------------------

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accessKey` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `secretKey` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `edition` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `level` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `copyright` smallint(6) NOT NULL,
  `thirdCopyright` smallint(6) NOT NULL,
  `hasStorage` smallint(6) NOT NULL,
  `hasProvidePermission` smallint(6) NOT NULL,
  `hasLive` smallint(6) NOT NULL,
  `hasMobiel` smallint(6) NOT NULL,
  `hasConsulting` smallint(6) NOT NULL,
  `mobileCode` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobileStartDate` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `mobileEndDate` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `siteName` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `siteUrl` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `licenseDomains` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `licenseIps` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `licenseName` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `licenseNumber` int(11) NOT NULL,
  `contact` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `phone` int(11) DEFAULT NULL,
  `mobile` bigint(11) NOT NULL,
  `qq` int(11) NOT NULL,
  `email` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `locationId` int(11) NOT NULL,
  `mobileLocation` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `region` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `source` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `salesman` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createdTime` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createdIp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updatedTime` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `isAppWhiteList` smallint(6) DEFAULT NULL,
  `levelName` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `accessCloud` tinyint(1) NOT NULL DEFAULT '1',
  `enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用',
  `locked` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否锁定',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('87', 'IEYe1QWh6DiRr4Ja7c5qYAUTPbCmUmeH', 'Le3JfVpYlBh5mh2uGF91UHvecFPxQLUu', '', '', '0', '0', '0', '0', '0', '0', '0', null, '', '', '', '', '', '', '', '0', '', null, '0', '0', '', '0', '', '', '', '', '', '', '', '1526805385', '', '1526805385', null, '', '1', '1', '0');

-- ----------------------------
-- Table structure for `videos`
-- ----------------------------
DROP TABLE IF EXISTS `videos`;
CREATE TABLE `videos` (
  `id` int(11) NOT NULL,
  `file` varchar(100) DEFAULT NULL,
  `cwd` varchar(50) DEFAULT NULL,
  `app` varchar(10) DEFAULT NULL,
  `vhost` varchar(30) DEFAULT NULL,
  `http_path` varchar(100) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `size` int(11) DEFAULT '0' COMMENT '文件大小 单位：字节 默认 0 获取大小有误',
  `duration` int(11) DEFAULT '0' COMMENT '视频时长 单位：秒',
  `stream` varchar(50) DEFAULT NULL,
  `cover` varchar(100) DEFAULT NULL,
  `remark` text CHARACTER SET utf8 COMMENT '视频备注信息',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of videos
-- ----------------------------
