<?php
namespace app\api\controller;

use app\api\controller\BaseApi;
use Think\Request;


class Oauth extends BaseApi
{
	public function _initialize()
	{
		parent::_initialize();
		$accessKey = $this->accessKey;
		$secretKey = get_secret_key($accessKey);
		$id = get_uid($accessKey, $secretKey);
		$controller = $this->request->controller();
		
		$auth = new \think\auth\Auth;
		$res = $auth->check($controller,$id);
		
		if (!$res){
			 $this->returnmsg(403,'permission denied');
		} 
		
	}

}