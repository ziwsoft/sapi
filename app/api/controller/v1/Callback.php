<?php

namespace app\api\controller\v1;

define('SRS_SUCCESS', 0);
define('SRS_ERROR', 1);

//验证相关的key无关
class Callback extends \think\Controller
{

    protected $data;
    protected $action;
    protected $ip;
    protected $vhost;
    protected $app;
    protected $client_id;
    protected $stream;
    protected $token;
    protected $tokenExpiret;

    protected function _initialize()
    {

        parent::_initialize();
        header("Content-Length:1");
        $data = file_get_contents("php://input");
        \think\Log::record(json_encode($data));
        if ($this->request->isPost() && empty($data)) {
            echo SRS_ERROR;
            exit;
        }
        $this->data = json_decode($data, true);

        $this->action = $this->data['action'];
        $this->app = $this->data['app'];
        $this->vhost = $this->data['vhost'];
        $this->ip = $this->data['ip'];
        $this->client_id = $this->data['client_id'];
        isset($this->data['stream']) && $this->stream = $this->data['stream'];
//        $this->token = $this->data['token'];
//        $this->tokenExpiret = $this->data['tokenExpiret'];
//        if (!$this->token || !$this->tokenExpiret) {
//            echo SRS_ERROR;
//            exit;
//        }
    }

    // 客户端推流
    public function onPublish()
    {
//        $now = time();
//        $stream = db("streams")->where(['stream' => $this->stream])->find();
//        if (empty($stream) || !$stream['status'] || ($stream['startTime'] > $now || $stream['endTime'] < $now)) {
//            return SRS_ERROR;
//        }
        return SRS_SUCCESS;
    }

    function onUnpublish()
    {
        return SRS_ERROR;
    }

    function onPlay()
    {
        return SRS_SUCCESS;
    }

    public function onStop()
    {
        return SRS_SUCCESS;
    }

    // 客户端连接
    function onConnect()
    {
//        $res = checkToken(
//                $this->app, $this->stream, $this->token, $this->tokenExpiret
//        );
//        if (!$res) {
//            echo SRS_ERROR;
//            exit;
//        }
//        unset($arr);
//        $arr['name'] = $this->stream;
//        $arr['app'] = $this->app;
//        $arr['vhost_id'] = $this->vhost;
//        $arr['client_id'] = $this->client_id;
//        $arr['tcurl'] = $this->data['tcUrl'];
//        $res = db('streams')->insert($arr);
        return SRS_SUCCESS;
    }

    function onClose()
    {
        return SRS_SUCCESS;
    }

    function onHLS()
    {
        try {
            $hls = \app\api\model\Fragment::create([
                        "ip" => $this->data['ip'], //"192.168.1.10",
                        "vhost" => $this->data['vhost'], //"video.test.com",
                        "app" => $this->data['app'], //"live",
                        "stream" => $this->data['stream'], // "livestream",
                        "duration" => $this->data['duration'], //9.36, // in seconds
                        "cwd" => $this->data['cwd'], // "/usr/local/srs",
                        "file" => $this->data['file'], //"./objs/nginx/html/live/livestream/2015-04-23/01/476584165.ts",
                        "url" => $this->data['url'], //"live/livestream/2015-04-23/01/476584165.ts",
                        "m3u8" => $this->data['m3u8'], // "./objs/nginx/html/live/livestream/live.m3u8",
                        "m3u8_url" => $this->data['m3u8_url'], // "live/livestream/live.m3u8",
                        "seq_no" => $this->data['seq_no']//
            ]);
            if (!$hls) {
                \think\Log::record($hls->getError(), \think\Log::ERROR);
            }
        } catch (\Exception $ex) {
            \think\Log::record($ex->getMessage(), \think\Log::ERROR);
        }

        return SRS_SUCCESS;
    }

    function onDvr()
    {

        try {
            $streamObj = \app\api\model\Stream::get(['stream' => $this->stream]);
            if (!empty($streamObj)) {

                $res = \app\api\model\Video::create([
                            'stream' => $this->stream,
                            'app' => $this->app,
                            'vhost' => $this->vhost,
                            'create_time' => time(),
                            'file' => $this->data ['file'],
                ]);
            }
            \think\Log::record($res, "debug");
        } catch (\Exception $ex) {

            \think\Log::record($ex->getMessage(), "error");
        }
        return SRS_SUCCESS;
    }

}
