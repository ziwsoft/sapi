<?php

namespace app\api\controller\v1;

use think\Controller;

class Notice extends Controller{

	public function getNotices(){
		$res = db('notices') -> where('status',1)->select();
		 return json($res);
	}
}