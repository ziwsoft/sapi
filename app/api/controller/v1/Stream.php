<?php

namespace app\api\controller\v1;

use \app\api\controller\BaseApi;

class Stream extends BaseApi {

    /**
     * 
     * @return type
     */
    public function create($start_time, $stop_time, $stream = "", $app = "live", $remark = "") {



        ($stop_time - $start_time < 60) && $this->returnMsg(400, "时长不能少于60秒");

        !empty($stream) && \app\api\model\Stream::destroy($stream);
        $streamObj = null;
        empty($stream) && $streamObj = \app\api\model\Stream::get([
                    'start_time' => $start_time,
                    'stop_time' => $stop_time,
                    'ip' => $this->request->ip()
        ]);

        !$streamObj && $streamObj = \app\api\model\Stream::create([
                    'start_time' => $start_time,
                    'stop_time' => $stop_time,
                    'app' => $app,
                    'user_id' => $this->uid,
                    'is_active' => 0,
                    'remark' => $remark,
                    'name' => random_str(32),
                    'ip' => $this->request->ip()
        ]);

        $streamObj === FALSE && $this->returnMsg(400, "创建流失败");

        $data = [
            'stream' => $streamObj->name,
            'push_url' => build_rtmp_url($streamObj->name,get_pushUrl($this->accessKey, $this->secretKey)),
            'start_time' => date("Y-m-d H:i:s", $streamObj->start_time),
            'stop_time' => date("Y-m-d H:i:s", $streamObj->stop_time),
        ];
        $hash = $this->buildHash($this->secretKey, $data);
        $data['sign'] = $hash;
        $this->returnMsg(200, "创建流成功", $data);
    }

    public function createLive($startTime, $endTime, $app = "live", $summary = "", $callback = [], $liveLogoUrl = [], $title = "", $speaker = "", $authUrl = "", $jumpUrl = "", $errorJumpUrl = "") {
        $streamObj = \app\api\model\Stream::create([
                    'startTime' => $startTime,
                    'endTime' => $endTime,
                    'app' => $app,
                    'user_id' => $this->uid,
                    'is_active' => 0,
                    'summary' => $summary,
                    'callback' => json_encode($callback),
                    'liveLogoUrl' => json_encode($liveLogoUrl),
                    'stream' => random_str(32),
                    'ip' => $this->request->ip(),
                    'authUrl' => $authUrl,
                    'jumpUrl' => $jumpUrl,
                    'errorJumpUrl' => $errorJumpUrl,
                    'speaker' => $speaker
        ]);

        !$streamObj && $this->returnMsg(400, "创建流失败");
      $pushUrl = get_pushUrl($this->accessKey,$this->secretKey);
        return json([
            'id' => $streamObj->id,
            'provider' => $this->uid,
            'stream' => build_rtmp_url($streamObj->stream,$pushUrl)
        ]);
    }

    public function updateLive($liveId, $startTime, $endTime, $summary = "", $callback = "", $liveLogoUrl = [], $title = "") {

        $streamObj = \app\api\model\Stream::get($liveId);
        !empty($summary) && $streamObj->summary = $summary;
        !empty($callback) && $streamObj->callback = $callback;
        !empty($liveLogoUrl) && $streamObj->liveLogoUrl = json_encode($liveLogoUrl);
        $streamObj->startTime = $startTime;
        $streamObj->endTime = $endTime;
        $streamObj->title = $title;

        $res = $streamObj->update();
        !$res && $this->returnmsg(400, "更新直播失败");
        return json([
            'success' => true
        ]);
    }

    public function removeLive($liveId, $provider) {

        $streamObj = \app\api\model\Stream::get($liveId);
        !$streamObj && $this->returnMsg(200);
        ($streamObj->user_id == $provider) && $streamObj->delete();
        $this->returnMsg(200);
    }

    public function isAvailableRecord($liveId) {

        return json([
            'success' => true
        ]);
    }

    public function getMaxOnline($liveId) {
        
    }

    public function entryRoom() {
        
    }

    public function gotoRoom($roomId, $ticketNo = "") {

        $streamObj = \app\api\model\Stream::get($roomId);

        !$streamObj && $this->returnMsg(400, "房间未找到");
        $ticket = \app\api\model\Ticket::get($ticketNo);
        !$ticket && $this->returnMsg(400, "Ticket未找到");
        $ticket->device == 'mobile' && $roomUrl = url("//", null, false, true) . "v1/play?roomId={$roomId}&ticket={$ticket->ticket}&tpl=hls";
        $ticket->device == 'desktop' && $roomUrl = url("//", null, false, true) . "v1/play?roomId={$roomId}&ticket={$ticket->ticket}";
        return json([
            'roomUrl' => $roomUrl,
            'host' => url("//", null, false, true)
        ]);
    }

}
