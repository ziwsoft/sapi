<?php

namespace app\api\controller\v1;

use app\api\controller\BaseApi;

class live extends BaseApi {

    public function index() {
        $liveInfo = db("live_info")->where(['uid' => $this->uid])->find();
        $arr = array();
        $arr['account'] = array(
            'capacity' => $liveInfo['capacity'],
            'effective' => $liveInfo['effective_time'],
            'expire' => $liveInfo['expire_time'],
            'message' => $liveInfo['message'],
            'renewInfo' => ['status' => $liveInfo['status']],
        );
        return json($arr);
      
    }

    public function createRecord($liveId, $title = "", $provider = "") {

        $stream = \app\api\model\Stream::get($liveId);
        !$stream && $this->returnMsg(400, "未找到相关流");
        $res = \app\api\model\Video::create([
                    'remark' => $title,
                    'provider' => $provider,
                    'room_id' => $liveId,
                    'stream' => $stream->stream,
                    'start_time' => time()
        ]);
    }

}
