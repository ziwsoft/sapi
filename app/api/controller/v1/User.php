<?php

namespace app\api\controller\v1;

use app\api\controller\BaseApi;
use think\Db;

class User extends BaseApi {

    /**
     * 
     * @return type
     */
    public function me() {
        $userInfo = db("users")->where(['accessKey' => $this->accessKey])->find();
        !$userInfo && $this->result(['code' => 400, 'msg' => '用户信息未找到']);
        unset($userInfo["id"], $userInfo['secretKey']);
        return json($userInfo);
    }

    public function ueserCenter($accessKey = "") {
        $cloudInfo = db("cloud_info")->where(['uid' => $this->uid])->find();
        !$cloudInfo && $this->result(['code' => 400, 'msg' => '用户信息未找到']);
        $arr = [];
        $arr['account'] = ['cash' => $cloudInfo['cash_account'], 'arrearageDays' => $cloudInfo['arrearage_days']];
        $arr['coupon'] = ['balance' => $cloudInfo['coupon_account']];
        if ($cloudInfo['access_cloud']) {
            $server_res = db("service_resource")->select();
            $server_list = isset($cloudInfo['service_list']) ? explode(',', $cloudInfo['service_list']) : null;
            $services = array();
            foreach ($server_res as $key => &$value) {
                $services[$value['services_name']] = in_array($value['id'], $server_list) ? 1 : 0;
            }
            $arr['service'] = $services;
        } else {
            $arr['service'] = ['accessCloud' => $cloudInfo['access_cloud']];
        }
        // dump($arr);die;
        return json($arr);
    }

    public function uesers($accessKey = "") {
        $cloudInfo = db("cloud_info")->where(['uid' => $this->uid])->find();
        !$cloudInfo && $this->result(['code' => 400, 'msg' => '用户信息未找到']);
        $userInfo = db("users")->where(['accessKey' => $accessKey])->find();
        unset($userInfo["id"], $userInfo['secretKey']);
        $arr = [];
        $arr['user'] = $userInfo;
        $arr['account'] = [
            'cash' => $cloudInfo['cash_account'],
            'arrearageDate' => $cloudInfo['arrearage_days'],
            'isRemind' => $cloudInfo['is_remind'],
            'updatedTime' => $cloudInfo['update_time']
        ];
        $arr['coupon'] = ['balance' => $cloudInfo['coupon_account']];
        if ($cloudInfo['access_cloud']) {
            $server_res = db("service_resource")->select();
            $server_list = isset($cloudInfo['service_list']) ? explode(',', $cloudInfo['service_list']) : null;
            $services = array();
            foreach ($server_res as $key => &$value) {
                $services[$value['services_name']] = in_array($value['id'], $server_list) ? 1 : 0;
            }
            $arr['service'] = $services;
        } else {
            $arr['service'] = ['accessCloud' => $cloudInfo['access_cloud']];
        }
        return json($arr);
    }

    public function verification($accessKey = "") {
          return json(['error' => 'can not change']);
    }

    public function serverList() {
        unset($arr);
        $arr = array();
        $arr['root'] = config("SERVICE_API");
        $arr['leafs'] = [
            ['url' => config("SERVICE_API")],
            ['url' => config("SERVICE_API")],
        ];
        $arr['current_leaf'] = config("SERVICE_API");
        return json($arr);
    }

    public function licenseDomain() {
        $res = db("users")->where(['id' => $this->uid])->value('licenseDomains');
        if ($res) {
             return json(['error' => 'License domain `' . $res . '` is already binded']);
        }
        $arr = $this->params;
        $id = db('users')->where(['id' => $this->uid])->update(['licenseDomains' => $arr['domain']]);
        if ($id) {
            return json(['licenseDomains' => $arr['domain']]);
        }
        
         return $this->returnMsg(400);
    }

}
