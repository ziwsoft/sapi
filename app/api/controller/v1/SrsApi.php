<?php

namespace app\api\controller\v1;

use think\Controller;
use \think\Db;

class SrsApi extends Controller {

    //获取服务器的摘要信息
    public function summaries() {
        return json(http_get(getSrsApi("summaries")));
    }

    //获取服务器的streams信息
    public function streams($streamid = 0) {
        $streamid ? $res = http_get(getSrsApi("streams/{$streamid}")) : $res = http_get(getSrsApi("streams/"));
        // dump($res);die;
        unset($arr);
        unset($data);
        $arr = array();
        foreach ($res['streams'] as $key => &$values) {
            $arr[$key]['server_id'] = $res['server'];
            $arr[$key]['stream_id'] = $values['id'];
            $arr[$key]['name'] = $values['name'];
            $arr[$key]['vhost'] = $values['vhost'];
            $arr[$key]['app'] = $values['app'];
            $arr[$key]['live_ms'] = $values['live_ms'];
            $arr[$key]['client_number'] = $values['clients'];
            $arr[$key]['frames'] = $values['frames'];
            $arr[$key]['publish_active'] = $values['publish']['active'];

            $data[$key]['stream_id'] = $values['id'];
            $data[$key]['send_bytes'] = $values['send_bytes'];
            $data[$key]['recv_bytes'] = $values['recv_bytes'];
            $data[$key]['kbps_recv_30s'] = $values['kbps']['recv_30s'];
            $data[$key]['kbps_send_30s'] = $values['kbps']['send_30s'];
            if ($values['video']) {
                $data[$key]['video_codec'] = $values['video']['codec'];
                $data[$key]['video_profile'] = $values['video']['profile'];
                $data[$key]['video_level'] = $values['video']['level'];
            }
            if ($values['audio']) {
                $data[$key]['audio_codec'] = $values['audio']['codec'];
                $data[$key]['audio_sample_rate'] = $values['audio']['sample_rate'];
                $data[$key]['audio_channel'] = $values['audio']['channel'];
                $data[$key]['audio_profile'] = $values['audio']['profile'];
            }
        }

        Db::startTrans();
        try {
            Db::table("api_streams")->insertAll($arr);
            Db::table("media_info")->insertAll($data);
            // 提交事务
            $res = true;
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            $res = false;
            echo 'error';
            Db::rollback();
        }
        if (!$res) {
            return json(['code' => 400, 'msg' => 'error']);
        }
        return json($res);
    }

    //获取服务器上的vhosts信息
    public function vhosts() {
        $data = http_get(getSrsApi("vhosts"));
        unset($arr);
        $arr = array();
        foreach ($data['vhosts'] as $key => $values) {
            // dump($values);die;
            $arr['server_id'] = $data['server'];
            $arr['vhost_id'] = $values['id'];
            $arr['name'] = $values['name'];
            $arr['enabled'] = isset($values['enabled']) ? 1 : 0;
            $arr['clients_number'] = $values['clients'];
            $arr['streams'] = $values['streams'];
            $arr['send_bytes'] = $values['send_bytes'];
            $arr['recv_bytes'] = $values['recv_bytes'];
            $arr['kbps_recv_30s'] = $values['kbps']['recv_30s'];
            $arr['kbps_send_30s'] = $values['kbps']['send_30s'];
            $arr['hls_enabled'] = isset($values['hls']['enabled']) ? 1 : 0;
            $arr['create_time'] = time();
        }
        $res = db('vhosts')->insert($arr);
        if ($res) {
            return json($arr);
        }
    }

    //获取服务器clients的信息，默认前十个
    public function clients() {
        $data = http_get(getSrsApi("clients"));
        unset($arr);
        $arr = array();
        foreach ($data['clients'] as $key => $values) {
            $arr['server_id'] = $data['server'];
            $arr['vhost'] = $values['vhost'];
            $arr['stream'] = $values['stream'];
            $arr['ip'] = $values['ip'];
            $arr['page_url'] = $values['pageUrl'];
            $arr['swf_url'] = $values['swfUrl'];
            $arr['tc_url'] = $values['tcUrl'];
            $arr['url'] = $values['url'];
            $arr['type'] = $values['type'];
            $arr['publish'] = $values['publish'];
            $arr['alive'] = $values['alive'];
            $arr['create_time'] = time();
        }
        $res = db('clients')->insert($arr);
        if ($res) {
            return json($arr);
        }
    }

    //开始录制
    ///api/v1/raw?rpc=update&scope=dvr&value=ossrs.net&param=enable&data=live/livestream
    public function startRecord($stream = "") {
        $streamObj = \app\api\model\Stream::get(['stream' => $stream]);
        if ($empty(streamObj)) {
            return json([
                'code'=>400,
                'msg'=>'未找到流信息'
            ]);
        }
        $result = http_get(getSrsApi("raw"), [
            'rpc' => 'update',
            'scope' => 'dvr',
            'value' => '__defaultVhost__',
            'param' => 'enable',
            'data' => $streamObj->app . '/' . $streamObj->stream
        ]);

        return json($result);
    }
    
      //停止录制
    ///api/v1/raw?rpc=update&scope=dvr&value=ossrs.net&param=enable&data=live/livestream
    public function stopRecord($stream = "") {
        $streamObj = \app\api\model\Stream::get(['stream' => $stream]);
        if ($empty(streamObj)) {
            return json([
                'code'=>400,
                'msg'=>'未找到流信息'
            ]);
        }
        $result = http_get(getSrsApi("raw"), [
            'rpc' => 'update',
            'scope' => 'dvr',
            'value' => '__defaultVhost__',
            'param' => 'disable',
            'data' => $streamObj->app . '/' . $streamObj->stream
        ]);

        return json($result);
    }

    /**
     * RAW 方法
     * @return type
     */
    public function raw() {

        // $this->request->get();
        $result = http_get(getSrsApi("raw"), $this->request->get());
        return json($result);
    }

    public function configs() {
        return json(http_get(getSrsApi("configs")));
    }

    //获取服务器的版本信息
    public function versions() {
        return json(http_get(getSrsApi("versions")));
    }

    //获取服务器资源使用信息
    public function rusages() {
        return json(http_get(getSrsApi("rusages")));
    }

    //获取服务器进程信息
    public function self_proc_stats() {
        return json(http_get(getSrsApi("self_proc_stats")));
    }

    //获取服务器所有进程信息
    public function system_proc_stats() {
        return json(http_get(getSrsApi("system_proc_stats")));
    }

    //获取服务器内存使用情况
    public function meminfos() {
        return json(http_get(getSrsApi("meminfos")));
    }

    //获取作者 版权和license信息
    public function authors() {
        return json(http_get(getSrsApi("authors")));
    }

    //获取系统支持的功能列表
    public function features() {
        return json(http_get(getSrsApi("features")));
    }

    //获取当前发起的详细请求信息
    public function requests() {
        return json(http_get(getSrsApi("requests")));
    }

}
