<?php

namespace app\api\controller\v1;

use \app\api\controller\BaseApi;

class Ticket extends BaseApi {
    /*
     * 生成推流凭证
     */

    public function createTicket($roomId, $nickname = "", $role = "", $hostname = "", $device = "", $avatar = "") {

        $stream = \app\api\model\Stream::get($roomId);

        $where['user_id'] = $this->uid;
        $where['nickname'] = $nickname;
        $where['roomId'] = $roomId;
        $where['hostname'] = $hostname;
        $where['device'] = $device;

        $ticketObj = \app\api\model\Ticket::get($where);
        if (!$ticketObj) {
            $ticketObj = \app\api\model\Ticket::create([
                        'user_id' => $this->uid,
                        'nickname' => $nickname,
                        'role' => $role,
                        'device' => $device,
                        'avatar' => $avatar,
                        'roomId' => $roomId,
                        'ticket' => random_str(32),
                        'hostname' => $hostname,
                        'create_time' => time(),
                        'expired_time' => $stream->endTime
            ]);
        }
        !$ticketObj && $this->returnMsg(400, "创建失败：{$ticketObj->getError()}");

        return json([
            'code' => 200,
            'no' => $ticketObj->id,
            'expired_time' => $stream->endTime
        ]);
    }

}
