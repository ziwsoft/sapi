<?php

namespace app\api\controller\v1;

use \app\api\controller\BaseApi;

class Cloud extends BaseApi {

    public function overview($accessKey = "") {

        $cloudInfo = db("cloud_info")->where(['uid' => $this->uid])->find();
        !$cloudInfo && $this->result(['code' => 400, 'msg' => '用户信息未找到']);
        $server_res = db("service_resource")->select();
        $server_list = isset($cloudInfo['service_list']) ? explode(',', $cloudInfo['service_list']) : null;
        $services = array();
        foreach ($server_res as $key => &$value) {
            $services[$value['services_name']] = in_array($value['id'], $server_list) ? 1 : 0;
        }
        $arr = [];
        $arr['accessCloud'] = $cloudInfo['access_cloud'];
        $arr['enabled'] = $cloudInfo['enabled'];
        $arr['locked'] = $cloudInfo['locked'];
        $arr['cashAccount'] = $cloudInfo['cash_account'];
        $arr['couponAccount'] = $cloudInfo['coupon_account'];
        $arr['services'] = $services;
        return json($arr);
    }

}
