<?php

namespace app\api\controller\v1;

//注册相关接口
class Regist extends \think\Controller {

    //创建Token
    public function create($email = "", $siteName = "", $siteUrl = "") {
        $result = ['code' => 1];
        empty($siteName) && $result = ['code' => 0, 'msg' => "[siteName]不能为空"];
        empty($siteUrl) && $result = ['code' => 0, 'msg' => "[siteUrl]不能为空"];
        empty($email) && $result = ['code' => 0, 'msg' => "[email]不能为空"];
        if (!$result['code']) {
            return json($result);
        }
        $user = \app\api\model\User::create([
                    'siteUrl' => $siteUrl,
                    'siteName' => $siteName,
                    'email' => $email,
                    'accessKey' => random_str(32),
                    'secretKey' => random_str(32)
        ]);
        if ($user) {
            return json([
                'accessKey' => $user->accessKey,
                'secretKey' => $user->secretKey
            ]);
        }

        return json([
            'code' => 0,
            'msg' => $user->getError()
        ]);
    }


}
