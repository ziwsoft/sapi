<?php

namespace app\api\controller;

/**
 * 用户相关接口
 */
class Index extends \think\Controller
{

    public function play($roomId, $ticket = "", $debug = false, $tpl = "")
    {

        $where['ticket'] = $ticket;
        $where["expired_time"] = ['>', time()];
        $ticketObj = db("tickets")->where($where)->order("create_time desc")->find();
        if (!$ticketObj) {
            $this->assign("error", "进入直播间失败：{$ticket} 已过期或不存在");
            return $this->fetch("error");
        }

        $streamObj = \app\api\model\Stream::get($roomId);
        if (!$streamObj) {
            $this->assign("error", "进入直播间失败：【未找到直播间：{$roomId}");

            return $this->fetch("error");
        }
        $this->assign("stream", $streamObj->stream);
        !$debug && $this->assign("rtmp_url", build_rtmp_url($streamObj->stream, get_pushUrlById($ticketObj['user_id'])));
        !$debug && $this->assign("m3u8_url", build_m3u8_url($streamObj->stream));
        if (empty($tpl)) {
            return $this->fetch("play");
        }
        return $this->fetch("play_{$tpl}");
    }

    public function playHLS($stream = "", $start = 0)
    {

        header("Content-Type:application/vnd.apple.mpegurl");
//        $seq = cache("seq_" . $this->request->ip());
//        $seq = $seq ? $seq : 0;
        $stream = substr($stream, 0, strlen($stream) - 5);
        $str = ""; //cache("{$stream}.m3u8");
        if (empty($str)) {
            $str = "#EXTM3U\n#EXT-X-VERSION:3\n#EXT-X-ALLOW-CACHE:YES\n #EXT-X-MEDIA-SEQUENCE:0\n#EXT-X-TARGETDURATION:5\n#EXT-X-DISCONTINUITY\n";
            $fragments = db("Fragments")->field("id,duration,file,url")->where(["stream" => $stream])->order("id asc")->select();
//            $fragments = array_slice($fragments, $seq);
            foreach ($fragments as &$frg) {
                $url = build_m3u8_ts($frg['url']);
                $str .= "\n#EXTINF:{$frg['duration']}, no desc\n{$url}";
            }
            
        }
        echo $str;
        exit;
    }

}
