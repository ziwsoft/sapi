<?php

namespace app\api\controller;

use think\Request;

class BaseApi extends \think\Controller {

    /**
     * 当前请求的authToken
     * @var [type]
     */
    private $authToken;

    /**
     * 当前请求的accessKey
     * @var [type]
     */
    protected $accessKey;

    /**
     * 数据查询出的secretK
     * @var [type]
     */
    protected $secretKey;

    /**
     * 当前请求的参数
     * @var [type]
     */
    public $params;

    /**
     * 当前请求类型
     * @var string
     */
    public $method;
    
    protected $uid;

    protected function _initialize() {
        $request = Request::instance();
        $this->request = $request;
        $this->params = $this->request->post();
        $this->method = $this->request->method();
        $this->checkToken();
    }

    /**
     * api 数据返回
     * @param  [int] $code [结果码 200:正常/4**数据问题/5**服务器问题]
     * @param  [string] $msg  [接口要返回的提示信息]
     * @param  [array]  $data [接口要返回的数据]
     * @return [string]       [最终的json数据]
     */
    protected function returnMsg($code, $msg = '', $data = '') {
        /*         * ********* 组合数据  ********** */
        $result = [
            'code' => $code,
            'msg' => $msg,
                //'time' => Request::instance()->server('REQUEST_TIME'),
                //   'data' => $data,
        ];
        $data && $result['data'] = $data;
        throw new \think\exception\HttpResponseException(json($result));
    }

    /**
     * 验证token(防止篡改数据)
     * @param  [array] $arr [全部请求参数]
     * @return [json]      [token验证结果]
     */
    private function checkToken() {

        $arr = $this->request->header();

        if (!isset($arr['auth-token']) || empty($arr['auth-token'])) {
            $this->returnmsg(401, 'AuthToken HTTP Header is not exist.');
        }
        //$app_token = $arr['auth-aoken']; // api传过来的token
        list($this->accessKey, $hash) = explode(":", $arr['auth-token']);
        $res = $this->buildHash($this->params);

        // print_r($res);
        if ($hash !== $res) {
            $this->returnMsg(400, 'token is error');
        }
    }

    public function buildHash($params = []) {
        $this->secretKey = get_secret_key($this->accessKey);
        !$this->secretKey && $this->returnMsg("403", "非法用户".$this->accessKey);
        $this->uid = get_uid($this->accessKey, $this->secretKey);
        $arr = $this->request->header();
        $url = $this->request->url(true);
        $matched = preg_match('/:\/\/.*?(\/.*)$/', $url, $matches);
        !$matched && $this->returnMsg(400, 'url is error');
        $this->request->isGet() && $params = array();
		ksort($params);
        //组装token里面的hash
        $text = $matches[1] . "\n" . json_encode($params) . "\n" . $this->secretKey;
        $hash = md5($text);
        return $hash;
    }

}
