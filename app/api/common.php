<?php

function random_str($length = 8) {

    // 密码字符集，可任意添加你需要的字符 
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $password = '';
    for ($i = 0; $i < $length; $i++) {
        $password .= $chars[mt_rand(0, strlen($chars) - 1)];
    }
    return $password;
}

function http_get($url, $params = []) {
    return json_decode(file_get_contents($url . '?' . http_build_query($params)), true);
}

function getSrsApi($api = "", $params = []) {

    $url = config("SRS_API") . $api;
    !empty($params) && $url .= '?' . http_build_query($params);
  //  think\Log::record($url,"debug");
    return $url;
}

function get_secret_key($accessKey = "") {
    $secretKey = cache("Cache_Key_{$accessKey}");
    if (!$secretKey) {
        $user = db("users")->where(["accessKey" => $accessKey])->field("secretKey")->find();
        $secretKey = $user['secretKey'];
        cache("Cache_Key_{$accessKey}", $secretKey);
    }
    return $secretKey;
}

function get_uid($accessKey = '', $secretKey = '') {
    $id = db("users")->where(["accessKey" => $accessKey, "secretKey" => $secretKey])->field("id")->find();
    $id = $id['id'];
    return $id;
}
//查找推流地址
function get_pushUrl($accessKey = '', $secretKey = '') {
    $pushUrl = db("users")->where(["accessKey" => $accessKey, "secretKey" => $secretKey])->field("pushUrl")->find();
    $pushUrl = $pushUrl['pushUrl'];
    return $pushUrl;
}

function get_pushUrlById($uid='') {
    $pushUrl = db("users")->where(["id" => $uid])->field("pushUrl")->find();
    $pushUrl = $pushUrl['pushUrl'];
    return $pushUrl;
}

function checkToken($app, $streamName, $token, $tokenExpiret) {

    $now = time();
    if ($now > $tokenExpiret) {
        return 0;
    }

    list($accessKey, $hash) = explode(":", $token);
    $secretKey = get_secret_key($accessKey);
    if (!$secretKey) {
        return 0;
    }
    $res = db('token_info')->where(["token" => $token, "stream" => $streamName])->find();
    $path = "/" . $app . "/" . $streamName . "?e=" . $tokenExpiret;
    $newhash = makeHash($path, $accessKey, $secretKey);
    if ($hash !== $newhash || !$res) {
        return 0;
    }

    return 1;
}

/**
 * 生成hash值
 * @param  [type] $path      [description]
 * @param  [type] $accessKey [description]
 * @param  [type] $secretKey [description]
 * @return [type]            [description]
 */
function makeHash($path, $accessKey, $secretKey) {
    $hash = hash_hmac('sha1', $path, $secretKey);
    return $hash;
}

function build_rtmp_url($stream = "session0", $pushUrl) {
    return sprintf('%s%s', $pushUrl, $stream);
}

function build_m3u8_url($stream = "session0") {
    return str_replace('{stream}', $stream, config("SRS_M3U8_SERVER"));
}


function build_m3u8_ts($url=""){
     return config("TS_SERVER").$url;
}