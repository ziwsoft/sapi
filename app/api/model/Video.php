<?php

namespace app\api\model;

use think\Model;

class Video extends Model {

    protected $table="videos";
    protected $autoWriteTimestamp = true; 
    protected $createTime="create_time";
    protected $updateTime="update_time";
}
