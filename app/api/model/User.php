<?php

namespace app\api\model;

use think\Model;

class User extends Model {

    protected $table="users";
    protected $autoWriteTimestamp = true; 
    protected $createTime="createdTime";
    protected $updateTime="updatedTime";
}
