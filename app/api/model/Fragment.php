<?php

namespace app\api\model;

use think\Model;

class Fragment extends Model
{

    protected $table = "fragments";
    protected $autoWriteTimestamp = true;
    protected $createTime = "create_time";
    protected $updateTime = "update_time";

}
