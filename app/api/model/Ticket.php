<?php

namespace app\api\model;

use think\Model;

class Ticket extends Model {

    protected $table="tickets";
    protected $autoWriteTimestamp = true; 
    protected $createTime="create_time";
    protected $updateTime=false;
}
