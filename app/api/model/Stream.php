<?php

namespace app\api\model;

use think\Model;

class Stream extends Model {

    protected $table="streams";
    protected $autoWriteTimestamp = true; 
    protected $createTime="create_time";
    protected $updateTime="update_time";
}