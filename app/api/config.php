<?php

//配置文件
return [
    'app_debug' => false,
    // 应用Trace
    'app_trace' => false,
    'SRS_API' => 'http://172.16.30.58:1985/api/v1/',
    'SRS_RTMP_SERVER' => 'rtmp://push.ziwsoft.com/live/',
    'SERVICE_API' => 'http://apidev2.ziwsoft.com/index.php',
    'SRS_M3U8_SERVER' => 'http://pull.ziwsoft.com/live/{stream}/vod_{stream}.m3u8',
     'TS_SERVER' => 'http://172.16.30.79:888/stream/',
    'auth' => [
        'auth_on' => 1, // 权限开关
        'auth_type' => 1, // 认证方式，1为实时认证；2为登录认证。
        'auth_group' => 'think_auth_group', // 用户组数据不带前缀表名
        'auth_group_access' => 'think_auth_group_access', // 用户-用户组关系不带前缀表
        'auth_rule' => 'think_auth_rule', // 权限规则不带前缀表
        'auth_user' => 'users', // 用户信息不带前缀表
    ],
];
