<?php

use think\Route;

//创建key
Route::post(":version/keys", "api/:version.Regist/create");


//获取用户个人信息
Route::get(":version/me", "api/:version.User/me");

Route::get(":version/cloud/:accessKey/overview", "api/:version.Cloud/overview");
Route::get(":version/user/center/:accessKey/overview", "api/:version.User/ueserCenter");
Route::get(":version/users/:accessKey/overview", "api/:version.User/uesers");
Route::get(":version/server_list", "api/:version.User/serverList");
Route::post(":version/keys/:accessKey/verification", "api/:version.User/verification");
Route::post(":version/me/license-domain", "api/:version.User/licenseDomain");

//获取公告信息
Route::get(":version/context/notice", "api/:version.Notice/getNotices");
//直播相关api接口
Route::get(":version/me/live/overview", "api/:version.Live/index");

Route::get(":version/db/:tbName", "api/DbBase/select");
Route::get(":version/db/:tbName/:id", "api/DbBase/find");


Route::get(":version/live/token/:expire", "api/:version.Token/createToken");

//创建直播，返回 {code:200,'"}
Route::post(":version/lives", "api/:version.Stream/createLive");

//更新直播
Route::patch(":version/lives/:liveId", "api/:version.Stream/updateLive");

//删除直播
Route::delete(":version/lives/:liveId", "api/:version.Stream/removeLive");

//是否可以录制
Route::get(":version/lives/:liveId/available_record", "api/:version.Stream/isAvailableRecord");

//获取最大在线人数
Route::get(":version/lives/:liveId/max_online", "api/:version.Stream/getMaxOnline");

//进入直播间 entry_room
Route::get(":version/lives/:liveId/entry_room", "api/:version.Stream/entryRoom");

//获取房间播放地址
Route::get(":version/liverooms/:roomId/tickets/:ticketNo", "api/:version.Stream/gotoRoom");

//创建ticket
Route::post(":version/liverooms/:roomId/tickets", "api/:version.Ticket/createTicket");
Route::get(":version/play", "api/Index/play");

Route::get(":version/vod/:stream", "api/Index/playHLS");


//Srs回调
Route::post(":version/on_publish", 'api/:version.Callback/onPublish');
Route::get(":version/on_publish", 'api/:version.Callback/onPublish');

Route::post(":version/on_unpublish", 'api/:version.Callback/onUnPublish');
Route::get(":version/on_unpublish", 'api/:version.Callback/onUnPublish');

Route::post(":version/on_play", 'api/:version.Callback/onPlay');
Route::get(":version/on_play", 'api/:version.Callback/onPlay');

Route::post(":version/on_stop", 'api/:version.Callback/onStop');
Route::get(":version/on_stop", 'api/:version.Callback/onStop');

Route::post(":version/on_connect", 'api/:version.Callback/onConnect');
Route::get(":version/on_connect", 'api/:version.Callback/onConnect');

Route::post(":version/on_close", 'api/:version.Callback/onClose');
Route::get(":version/on_close", 'api/:version.Callback/onClose');

Route::post(":version/on_hls", 'api/:version.Callback/onHLS');
Route::get(":version/on_hls", 'api/:version.Callback/onHLS');

Route::post(":version/dvrs", 'api/:version.Callback/onDvr');
Route::get(":version/dvrs", 'api/:version.Callback/onDvr');


//Srs Http-API
Route::get(":version/summaries", "api/:version.SrsApi/summaries");
Route::get(":version/streams/:streamid", "api/:version.SrsApi/streams");
Route::get(":version/streams", "api/:version.SrsApi/streams");
Route::get(":version/versions", "api/:version.SrsApi/versions");
Route::get(":version/rusages", "api/:version.SrsApi/rusages");
Route::get(":version/self_proc_stats", "api/:version.SrsApi/self_proc_stats");
Route::get(":version/system_proc_stats", "api/:version.SrsApi/system_proc_stats");
Route::get(":version/meminfos", "api/:version.SrsApi/meminfos");
Route::get(":version/authors", "api/:version.SrsApi/authors");
Route::get(":version/features", "api/:version.SrsApi/features");
Route::get(":version/requests", "api/:version.SrsApi/requests");
Route::get(":version/vhosts", "api/:version.SrsApi/vhosts");
Route::get(":version/clients", "api/:version.SrsApi/clients");
Route::get(":version/configs", "api/:version.SrsApi/configs");

Route::get(":version/startRecord", "api/:version.SrsApi/startRecord");

Route::get(":version/raw", "api/:version.SrsApi/raw");

//返回错误页面
Route::miss('Error/index');

//系统演示路由
Route::get("","index/Index/index");
Route::get("config","index/Index/config");

Route::post("config","index/Index/config");

