<?php

namespace app\index\controller;

use think\Controller;

class Index extends Controller {

    public function index() {
        return $this->fetch(":index");
    }

    public function config() {

        if ($this->request->isGet()) {
            return $this->fetch(":config");
        }
        cache("channel_first", input("channel_first"));
          cache("channel_second", input("channel_second"));
            cache("channel_main", input("channel_main"));
            $this->success("配置成功");
        
    }

}
