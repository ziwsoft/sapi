<?php
namespace app\index\controller;

use think\Request;

class Error
{
    public function index()
    {	

        return json(array('error'=>405,'message'=>'路由或请求方式有误，请走正确的路线'));
    }
}