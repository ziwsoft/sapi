<?php

return [
    'view_replace_str' => [
        '__PUBLIC__' => '/static'
    ],
    'template' => [
        'layout_on' => false,
        'layout_name' => 'layout',
    ]
];
